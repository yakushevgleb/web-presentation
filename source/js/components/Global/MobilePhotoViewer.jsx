import React, { Component } from 'react';
import Config from 'Config';
import ReactSwipe from 'react-swipe'

import blockPlansIcon from "../../../assets/img/icons/block_plans.png";
import blockPhotosIcon from "../../../assets/img/icons/block_photos.png";

export default class MobilePhotoViewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      position: 1,
      dynamicWidth: null,
      dynamicHeight: null,
      imageLoader: 'not loaded'
    }
    this.resizeViewer = this.resizeViewer.bind(this);
    this.updatePosition = this.updatePosition.bind(this);
    this.handleImageLoad = this.handleImageLoad.bind(this);
  }

  resizeViewer() {
    let photoArea = document.getElementById(this.props.type);
    this.setState({
      dynamicWidth: photoArea.clientWidth,
      dynamicHeight: photoArea.clientHeight
    });
  }

  componentDidMount() {
    window.addEventListener("resize", this.resizeViewer);
    this.resizeViewer();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.resizeViewer);
  }

  updatePosition() {
    this.setState({
      position: this.reactSwipe.getPos() + 1
    });
  }

  handleImageLoad() {
    this.setState({
      imageLoader: 'loading'
    });
  }

  render() {

    return (
      <div className='MobilePhotoViewer' id={this.props.type}>
        <ReactSwipe key={this.props.images.length} ref={(reactSwipe)=>{this.reactSwipe = reactSwipe;}} className="carousel" swipeOptions={{continuous: true}}>
          {this.props.images.map((image, index)=>

              <div className="image-block" key={index}>
                <img
                  onTouchEnd={this.updatePosition}
                  onMouseUp={this.updatePosition}
                  onLoad={this.handleImageLoad}
                  src={`${Config.address}/api/presentations/data/image_crop?url=${Config.photoAddress + image}&width=${this.state.dynamicWidth}&height=${this.state.dynamicHeight}`}
                />
              </div>

          )}
        </ReactSwipe>
        <div className="photo-counter">{this.state.position}/{this.props.images.length}</div>
      </div>
    );
  }
}
