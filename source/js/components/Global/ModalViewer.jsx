import React, { Component } from 'react';
import Config from 'Config';
import ReactSwipe from 'react-swipe'

export default class ModalViewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalOpened: false,
      images: [],
      currentPosition: this.props.position,
      perfomanceArr: [],
      startSlide: 0,
      separatedIndex: 0
    };
  }

  componentWillMount() {

    let images = [];
    this.props.images.forEach((el, index)=>{
      if (index === this.props.position) {
        images.push({
          url: Config.photoAddress + el,
          choosen: "choosen"
        });
      } else {
        images.push({
          url: Config.photoAddress + el,
          choosen: null
        });
      }
    });

    let position = this.props.position;
    let perfomanceArr = this.arrayFiller(position, images);

    this.setState({
      images,
      perfomanceArr
    });
  }


  changePhoto(dir) {
    let {perfomanceArr, images, currentPosition} = this.state;
    if(dir === "prev") {
      this.reactSwipe.prev();
      (currentPosition === 0) ? currentPosition : currentPosition--;
      images[currentPosition].choosen = "choosen";
      images[currentPosition + 1].choosen = null;
      if(currentPosition !== 0 && perfomanceArr.length > 2) {
        perfomanceArr[currentPosition - 1] = images[currentPosition - 1].url;
        perfomanceArr[currentPosition + 2] = "nope";
      }
    } else if (dir === "next") {
      this.reactSwipe.next();
      (currentPosition === images.length - 1) ? currentPosition : currentPosition++;
      images[currentPosition].choosen = "choosen";
      images[currentPosition - 1].choosen = null;
      if(currentPosition !== images.length - 1 && images.length > 2) {
        perfomanceArr[currentPosition + 1] = images[currentPosition + 1].url;
        perfomanceArr[currentPosition - 2] = "nope";
      }
    }

    this.setState({
      images,
      currentPosition,
      perfomanceArr
    });
  }

  arrayFiller(position, oldArr) {
    let newArr = [];
    if (oldArr.length > 3) {
      oldArr.forEach(item=>{
        newArr.push("nope");
      });
      if (position === 0) {
        newArr[position] = oldArr[position].url;
        newArr[position + 1] = oldArr[position + 1].url;
        newArr[position + 2] = oldArr[position + 2].url;
      } else if (position === oldArr.length - 1) {
        newArr[position - 2] = oldArr[position - 2].url;
        newArr[position - 1] = oldArr[position - 1].url;
        newArr[position] = oldArr[position].url;
      } else {
        newArr[position - 1] = oldArr[position - 1].url;
        newArr[position] = oldArr[position].url;
        newArr[position + 1] = oldArr[position + 1].url;
      }
    } else {
      oldArr.forEach(image=>{
        newArr.push(image.url);
      });
    }
    return newArr;
  }

  openModal() {
    this.props.openModal();
  }

  chooseImage(index) {
    let {images, perfomanceArr, currentPosition} = this.state;
    perfomanceArr = this.arrayFiller(index, images);
    images[currentPosition].choosen = null;
    images[index].choosen = "choosen";
    this.setState({
      currentPosition: index,
      images,
      perfomanceArr
    });
    this.reactSwipe.slide(index, 300);
  }



  render() {

    return (
      <div>
        <div className="modal">
          <div className="modal-background">
          </div>
          <div className="original-photo">
            <ReactSwipe
            key={this.props.images.length}
            ref={(reactSwipe)=>{this.reactSwipe = reactSwipe;}}
            className="carousel"
            swipeOptions={{
              continuous: false,
              startSlide: this.props.position
            }}>
              {this.state.perfomanceArr.map((image, index)=>

                <img key={index} src={image}/>

              )}
            </ReactSwipe>
            {this.props.images.length > 1 &&
              <div className="left-button">
                <svg onClick={this.changePhoto.bind(this, "prev")} className="photo-button" width="100%" height="100%" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
                  <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
                  <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
                  <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
                </svg>
              </div>
            }
            {this.props.images.length > 1 &&
              <div className="right-button">
                <svg onClick={this.changePhoto.bind(this, "next")} className="photo-button" width="100%" height="100%" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
                  <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
                  <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
                  <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
                </svg>
              </div>
            }
          </div>
          <div className="previews-area">
            {this.state.images.map((image, index)=>
              <div key={index} className={`image-block ${image.choosen}`} onClick={this.chooseImage.bind(this, index)}>
                <img src={`${Config.address}/api/presentations/data/image_crop?url=${image.url}&width=105&height=72`} />
              </div>
            )}
          </div>
          <div className="close-area">
            <span className="close thin" onClick={this.openModal.bind(this)}>+</span>
          </div>
        </div>
      </div>
    );
  }
}
