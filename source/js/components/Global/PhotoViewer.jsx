import React, { Component } from 'react';
import Config from 'Config';
import ReactSwipe from 'react-swipe'

import blockPlansIcon from "../../../assets/img/icons/block_plans.png";
import blockPhotosIcon from "../../../assets/img/icons/block_photos.png";

export default class PhotoViewer extends Component {

  state = {
    modalOpened: false
  }

  componentDidMount() {

  }

  changePhoto(dir) {
    if(dir === "prev") {
      this.reactSwipe.prev();
    } else if (dir === "next") {
      this.reactSwipe.next();
    }
    this.props.currentPosition(this.reactSwipe.getPos());
  }

  chooseImage(index) {
    let currentPosition = this.state.currentImage.position;
    let images = this.state.images;
    images[currentPosition].choosen = null;
    images[index].choosen = "choosen";
    this.setState({
      currentImage: {
        url: Config.photoAddress + this.props.images[index],
        position: index
      },
      images: images
    });
  }



  render() {

    return (
      <div>
        <div className='PhotoViewer'>
          <ReactSwipe key={this.props.images.length} ref={(reactSwipe)=>{this.reactSwipe = reactSwipe;}} className="carousel" swipeOptions={{continuous: true}}>
            {this.props.images.map((image, index)=>

                <div className="image-block" key={index}>
                  <img
                    onTouchEnd={this.updatePosition}
                    onMouseUp={this.updatePosition}
                    onLoad={this.handleImageLoad}
                    src={`${Config.address}/api/presentations/data/image_crop?url=${Config.photoAddress + image}&width=495&height=300`}
                  />
                </div>

            )}
          </ReactSwipe>
          {this.props.images.length > 1 &&
            <div className="left-button">
              <svg onClick={this.changePhoto.bind(this, "prev")} className="photo-button" width="100%" height="100%" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
                <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
                <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
                <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
              </svg>
            </div>
          }
          {this.props.images.length > 1 &&
            <div className="right-button">
              <svg onClick={this.changePhoto.bind(this, "next")} className="photo-button" width="100%" height="100%" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
                <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
                <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
                <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
              </svg>
            </div>
          }
        </div>
      </div>
    );
  }
}
