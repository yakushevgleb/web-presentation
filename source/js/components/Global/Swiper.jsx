import React, { Component } from 'react';
import Config from 'Config';

import blockPlansIcon from "../../../assets/img/icons/block_plans.png";
import blockPhotosIcon from "../../../assets/img/icons/block_photos.png";

export default class Swiper extends Component {

  constructor(props) {
    super(props);

    this.state = {
      left: 0,
      startLeft: 0,
      originalOffset: 0,
      velocity: 0,
      timeOfLastDragEvent: 0,
      touchStartX: 0,
      prevTouchX: 0,
      beingTouched: false,
      intervalId: null,
      position: 1
    };
  }

  animateSlidingToZero() {
    let {left, velocity, beingTouched, position, startLeft} = this.state;
    if (!beingTouched && left < startLeft - 0.01 && position < this.props.length) {

      if (left < startLeft - 200) {
        velocity -= 10 * 0.033;
        left += velocity;

        if (left <= startLeft - this.props.imageWidth) {
          window.clearInterval(this.state.intervalId);
          this.setState({
            left: startLeft - this.props.imageWidth,
            startLeft: startLeft - this.props.imageWidth,
            velocity,
            position: position + 1
          });
          this.updatePosition();
        } else {
          this.setState({left, velocity});
        }

      } else {
        left += velocity;
        this.setState({left, velocity});
      }

    } else if (!beingTouched && left > startLeft + 0.01 && position > 1) {

      if (left > startLeft + 200) {
        velocity += 10 * 0.033;
        left += velocity;

        if (left >= startLeft + this.props.imageWidth) {
          window.clearInterval(this.state.intervalId);
          this.setState({
            left: startLeft + this.props.imageWidth,
            startLeft: startLeft + this.props.imageWidth,
            velocity,
            position: position - 1});
          this.updatePosition();
        } else {
          this.setState({left, velocity});
        }

      } else {
        left += velocity;
        this.setState({left, velocity});
      }

    } else if (!beingTouched && position === this.props.length) {
      left = startLeft;
      velocity = 0;
      window.clearInterval(this.state.intervalId);
      this.setState({left, startLeft, velocity, intervalId: null, originalOffset: 0});
    } else if (!beingTouched) {
      left = 0;
      velocity = 0;
      startLeft = 0;
      window.clearInterval(this.state.intervalId);
      this.setState({left, startLeft, velocity, intervalId: null, originalOffset: 0});
    }
  }

  updatePosition() {
    this.props.updatePosition(this.state.position);
  }

  handleStart(clientX) {
    if (this.state.intervalId !== null) {
      window.clearInterval(this.state.intervalId);
    }
    this.setState({
      originalOffset: this.state.left,
      velocity: 0,
      timeOfLastDragEvent: Date.now(),
      touchStartX: clientX,
      beingTouched: true,
      intervalId: null
    });
  }

  handleMove(clientX) {
    if (this.state.beingTouched) {
      const touchX = clientX;
      const currTime = Date.now();
      const elapsed = currTime - this.state.timeOfLastDragEvent;
      const velocity = 20 * (touchX - this.state.prevTouchX) / elapsed;
      let deltaX = touchX - this.state.touchStartX + this.state.originalOffset;
      this.setState({
        left: deltaX,
        velocity,
        timeOfLastDragEvent: currTime,
        prevTouchX: touchX
      });
    }
  }

  handleEnd() {
    this.setState({
      velocity: this.state.velocity,
      touchStartX: 0,
      beingTouched: false,
      intervalId: window.setInterval(this.animateSlidingToZero.bind(this), 15)
    });
  }

  handleTouchStart(touchStartEvent) {
    this.handleStart(touchStartEvent.targetTouches[0].clientX);
  }

  handleTouchMove(touchMoveEvent) {
    this.handleMove(touchMoveEvent.targetTouches[0].clientX);
  }

  handleTouchEnd() {
    this.handleEnd();
  }

  handleMouseDown(mouseDownEvent) {
    mouseDownEvent.preventDefault();
    this.handleStart(mouseDownEvent.clientX);
  }

  handleMouseMove(mouseMoveEvent) {
    this.handleMove(mouseMoveEvent.clientX);
  }

  handleMouseUp() {
    this.handleEnd();
  }

  handleMouseLeave() {
    this.handleMouseUp();
  }

  render() {
    return (
      <div className="Swiper"
        style={{"left": this.state.left + 'px'}}
        onTouchStart={touchStartEvent => this.handleTouchStart(touchStartEvent)}
        onTouchMove={touchMoveEvent => this.handleTouchMove(touchMoveEvent)}
        onTouchEnd={() => this.handleTouchEnd()}
      >
        {this.props.children}
      </div>
    );
  }
}
