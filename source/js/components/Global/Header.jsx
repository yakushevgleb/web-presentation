import React, { Component } from 'react';

import logo from "../../../assets/img/logo.png"

export default class Header extends Component {

  componentDidMount() {
    let header = document.querySelector('.Header');
    let pageTitle = document.querySelector('.mobile-page-title');
    let origOffsetY = header.getBoundingClientRect();
    let headerIsFixed = false;

    function onScroll(e) {
      if(window.scrollY >= origOffsetY.top != headerIsFixed) {
        headerIsFixed = !headerIsFixed;
        header.classList.toggle('sticky', headerIsFixed);
        if (pageTitle) {
          pageTitle.classList.toggle('add-margin', headerIsFixed);
        }
      }
    }

    document.addEventListener('scroll', onScroll);

  }

  render() {
    return (
      <div className="Header" style={this.props.sticky ? {"position": "fixed"} : {}}>
        <img src={logo} className="logo" /> <a className="phone-number phone bold" href={`tel:${this.props.phone}`}>{this.props.phone}</a>
      </div>
    );
  }
}
