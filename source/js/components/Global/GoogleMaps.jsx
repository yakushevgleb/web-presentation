import React, { Component } from 'react';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

const MyGoogleMap = withGoogleMap(props => (
  <GoogleMap
    ref={props.onMapLoad}
    zoom={15}
  >
    {props.markers.map((marker, index) => (
      <Marker key={index}
        {...marker}
      />
    ))}
  </GoogleMap>
));

export default class GoogleMaps extends Component {

  onMapLoad = this.onMapLoad.bind(this);

  onMapLoad(map) {
    this._mapComponent = map;
    let bounds = new google.maps.LatLngBounds();
    this.props.markers.forEach(marker=>{
      let latLng = new google.maps.LatLng(marker.position.lat, marker.position.lng);
      bounds.extend(latLng);
    });
    if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
       var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
       var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
       bounds.extend(extendPoint1);
       bounds.extend(extendPoint2);
    }
    if (map) {
      map.fitBounds(bounds);
    }
  }

  render() {

    let markers = this.props.markers;

    return (
      <div style={{height: `100%`}}>
      <MyGoogleMap
        containerElement={
          <div style={{ height: `100%` }} />
        }
        mapElement={
          <div style={{ height: `98%` }} />
        }
        onMapLoad={this.onMapLoad}
        markers={this.props.markers}
      />
      </div>
    );
  }
}
