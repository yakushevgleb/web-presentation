import React, { Component } from 'react';
import phone from '../../../assets/img/icons/phone.png'
import human from '../../../assets/img/icons/human.png'
import logo from "../../../assets/img/logo.png"

export default class TextDate extends Component {

  constructor(props) {
    super(props);
    this.state = {pageCounter: 1}
  }

  componentDidMount() {
    this.setState({pageCounter: this.props.pageCounter})
  }

  render() {
    let date = new Date();
    let month = date.getMonth();
    let fullYear = date.getFullYear();
    switch(month){
      case 0:
        month = "Январь";
        break;
      case 1:
        month = "Февраль";
        break;
      case 2:
        month = "Март";
        break;
      case 3:
        month = "Апрель";
        break;
      case 4:
        month = "Май";
        break;
      case 5:
        month = "Июнь";
        break;
      case 6:
        month = "Июль";
        break;
      case 7:
        month = "Август";
        break;
      case 8:
        month = "Сентябрь";
        break;
      case 9:
        month = "Октябрь";
        break;
      case 10:
        month = "Ноябрь";
        break;
      case 11:
        month = "Декабрь";
        break;
    }

    date = `${month} ${fullYear}`;


    return (
      <span className='TextDate'>
        {date}
      </span>
    );
  }
}
