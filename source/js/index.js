import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import 'babel-polyfill';
import logger from 'dev/logger';
import { Router, Route, Switch, browserHistory } from 'react-router'

import rootReducer from 'reducers';
import Routes from 'routes';

import DesktopApp from 'views/Desktop/App';
import MobileApp from 'views/Mobile/App';

import '../scss/app.scss';

function windowWidth() {
  if (screen.width >= 1024) {
    return (
      <Router history={browserHistory} >
        <Route path="/:id" component={DesktopApp} />
      </Router>
    );
  } else {
    return (
      <Router history={browserHistory} >
        <Route path="/:id" component={MobileApp}>
          <Route path="test" component={MobileApp} />
        </Route>
      </Router>
    );
  }
}

ReactDOM.render(
  windowWidth(),
  document.getElementById('root')
);
