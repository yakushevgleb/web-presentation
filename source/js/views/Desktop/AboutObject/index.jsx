import React, { Component } from 'react';
import Config from 'Config';

import GoogleMaps from "../../../components/Global/GoogleMaps";
import PhotoViewer from "../../../components/Global/PhotoViewer";
import ModalViewer from "../../../components/Global/ModalViewer";

import bigBld from "../../../../assets/img/icons/big_bld.png";
import addrIcon from "../../../../assets/img/icons/addr.png";
import metroIcon from "../../../../assets/img/icons/metro.png";
import bluePath from "../../../../assets/img/icons/blue_path.png";
import abc from "../../../../assets/img/icons/abc.png";
import dogovor from "../../../../assets/img/icons/dogovor.png";
import gear from "../../../../assets/img/icons/gear.png";
import goal from "../../../../assets/img/icons/goal.png";
import lift from "../../../../assets/img/icons/lift.png";
import layer from "../../../../assets/img/icons/layer.png";
import pdf from "../../../../assets/img/icons/pdf.png";
import photo from "../../../../assets/img/icons/photo.png";
import check from "../../../../assets/img/icons/v.png";
import pen from "../../../../assets/img/icons/pen.png";
import plus from "../../../../assets/img/icons/plus.png";
import parkingIcon from "../../../../assets/img/icons/parking.png";
import floorsIcon from "../../../../assets/img/icons/bld_icon.png";
import craneIcon from "../../../../assets/img/icons/crane.png";
import rulerIcon from "../../../../assets/img/icons/ruler.png";
import taxIcon from "../../../../assets/img/icons/tax_icon.png";
import smallMapIcon from "../../../../assets/img/icons/small_map_icon.png";
import blockPhotosIcon from "../../../../assets/img/icons/block_photos.png";
import blockPlansIcon from "../../../../assets/img/icons/block_plans.png";

export default class AboutObject extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showText: true,
      propsArr: [],
      modalOpened: false,
      images: null,
      position: 0,
      storePosition: false,
      marker: [{
        position: {
        lat: 55.76,
        lng: 37.64
        },
        icon: smallMapIcon
      }]
    }
    this.currentPosition = this.currentPosition.bind(this);
  }

  componentDidMount() {
    if(this.props.model.infrastructures) {
      this.setState({showText: true});
    } else {
      this.setState({showText: false});
    }
    this.setState({
      propsArr: [
        {
          value: this.props.model.bc_class,
          icon: abc,
          fieldName: "bc_class",
          propName: "Класс"
        },
        {
          value: this.props.model.year,
          icon: craneIcon,
          fieldName: "year",
          propName: "Год постройки"
        },
        {
          value: this.props.model.area,
          icon: goal,
          fieldName: "area",
          propName: "Общая площадь"
        },
        {
          value: this.props.model.floors,
          icon: floorsIcon,
          fieldName: "floors",
          propName: "Этажность"
        },
        {
          value: (this.props.model.lift_company == "1") ? "Лифт" : this.props.model.lift_company,
          icon: lift,
          fieldName: "lift_company",
          propName: this.props.model.passenger_elevators
        },
        {
          value: this.props.model.land_area,
          icon: rulerIcon,
          fieldName: "land_area",
          propName: "Площадь участка"
        },
        {
          value: this.props.model.rent_contract,
          icon: dogovor,
          fieldName: "rent_contract",
          propName: "Договор аренды"
        },
        {
          value: this.props.model.taxes_department,
          icon: taxIcon,
          fieldName: "taxes_department",
          propName: "Налоговая"
        }
      ],
      marker: [{
        position: {
        lat: parseFloat(this.props.model.coords.lat),
        lng: parseFloat(this.props.model.coords.lon)
        },
        icon: smallMapIcon
      }]
    })
  }

  openModal(images, storePosition) {

    this.setState({
      storePosition: storePosition
    });

    if (images) {
      this.setState({
        images: images
      });
    }
    this.setState({
      modalOpened: !this.state.modalOpened
    });
  }

  currentPosition(position) {
    this.setState({
      position: position
    });
  }

  downloadPresentation(){
    window.open(`${Config.address}/api/presentations/data/new_pdf?id=${this.props.presId}&building_id=${this.props.index}`)
  }

  render() {
    let index = this.props.index + 1;
    let model = this.props.model;
    let blocks = this.props.blocks;
    let infrastructures = model.infrastructures.split(',');
    let parking = model.parking_json;

    function currencySign(val) {
      switch (val) {
        case 1:
          return (<span>руб.</span>);
        case 2:
          return (<span>&euro;</span>);
        case 3:
          return (<span>$</span>);
        default:
          return (<span>руб.</span>);

      }
    }

    function sortImages(arr) {
      let sortedArr = [];
      arr.forEach(el=>{
        if (el.show) {
          sortedArr.push(el.url);
        }
      });
      return sortedArr;
    };


    return (
      <div className="AboutObject" id={`object${this.props.index}`}>
        {this.state.modalOpened &&
          <ModalViewer images={this.state.images} openModal={this.openModal.bind(this, null)} position={((this.state.storePosition) ? this.state.position : 0)} />
        }
        <div className="header">
          <div className="logo">
            <img src={bigBld} />
            <span>№{index}</span>
          </div>
          <div className="title-area">
            <div className="title thin"><span>{this.props.model.name}</span></div>
            <div className="address thin"><img src={addrIcon} /><span>{this.props.model.address}</span></div>
            <div className="object-type"><span className="bold">Тип объекта</span> <span className="bc">{this.props.model.prefix}</span></div>
          </div>
          <div className="metro-area">
            {model.stations.map((station, index)=>{
              return (
                <div className="metro" key={index}>
                  <div className="name-logo">
                    <img src={metroIcon} /> <span className="thin">{station.name}</span>
                  </div>
                  <div className="info">
                    <img src={bluePath} /><img src={addrIcon} /><span className="thin">{station.time} мин {station.type}</span>
                  </div>
                </div>
            )
            })}
          </div>
        </div>

        <div className="object-info">
          <div className="image">
            {model.images.length ? (
              <PhotoViewer images={model.images} currentPosition={this.currentPosition} />
            ) : (
              <div></div>
            )
            }
            {model.images.length ? (
              <div className="enlarge-photo" onClick={this.openModal.bind(this, model.images, true)}><span className="enlarge-icon">+</span><span className="tooltip">увеличить фото</span></div>
            ) : (
              <div></div>
            )
            }
          </div>
          <div className="properties">
           <div className="props">

           {this.state.propsArr.map((prop, index)=>{
             if(prop.value) {
               return (<div className="prop" key={index}>
                          <div className="prop-image"><img src={prop.icon} /></div>
                           <div className="prop-name">
                             <div className="prop-name-value"><span>{prop.value}</span></div>
                             <div className="prop-name-text">{prop.propName}</div>
                           </div>
                         </div>)
             }

           })}

           </div>


           <div className="infrastructure">
            <div className="infrastructure-title">
              <img src={check} /> <span>Инфраструктура</span>
                <div className="infrastructure-values" onClick={this.showInp}>
                  {infrastructures.map((item, index)=>{
                    return <span key={index}>{item}</span>
                  })}
                </div>
            </div>
           </div>
          </div>
          <div className="description">
            <div className="technical">
             <div className="technical-title">
              <img src={gear} /> <span>Технические характеристики</span>
             </div>
             {model.features.map((feature, index)=>{
               return (<div className="feature" key={index}>
                        <span>{feature.key}</span>
                        <span className="val">
                          {this.props.model.features[index].val}
                        </span>
                      </div>)
             })}
            </div>
            <div className="commercial">
              <div className="commercial-title">
                <img src={pen} /> <span>Коммерческие условия</span>
              </div>
              <div className="parking">
                {model.offer.map((item, index)=>{
                  if(item.val !== null) {
                    return (
                      <div key={index}>
                        {item.key}
                        <span className="blue-text"> {item.val}</span>
                      </div>)
                  }
                }
                )}
              </div>
              <div className="parking">
                <div className="parking-title">Паркинг:</div>
                {parking.map((item, index)=>{
                  return (<span className="parking-item" key={index}>
                            <img src={parkingIcon} />
                            <span className="blue-text"> {item.val}</span>
                            {item.price ? (
                              <span> {item.price} {currencySign(item.cur)}/мес.</span>
                            ) : (
                              <span></span>
                            )}
                          </span>)
                })}
              </div>
            </div>
          </div>
        </div>

        <div className="object-info">
          <div className="image">
            <GoogleMaps markers={this.state.marker}/>
          </div>
          <div className="suggested-areas">
            <div className="suggested-areas-title">
              <img src={plus} /> <span>Предлагаемые площади</span>
            </div>
            <div className="suggested-areas-table-header">
              <table>
                <tbody>
                  <tr className="table-header">
                    <td className="table-floor">Этаж</td>
                    <td className="table-area">Площадь м<sup>2</sup></td>
                    <td className="table-price">{(this.props.orderType == "rent") ? (<span>Ставка р./м<sup>2</sup> в год</span>) : (<span>Стоимость р./м<sup>2</sup></span>)}</td>
                    {this.props.orderType == "rent" &&
                      <td className="table-expense">Эксп. расх. р./м<sup>2</sup>  в год</td>
                    }
                    <td className="table-tax">Налоги</td>
                    <td className="table-comment">Комментарий</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="suggested-areas-table-content">
              <table>
                <tbody>
                  {blocks.map((block, index)=>{
                    return (<tr className="table-content" key={index}>
                              <td className="table-floor">
                                {block.floors}
                                {sortImages(block.images).length ? (
                                  <div className="block-icon" onClick={this.openModal.bind(this, sortImages(block.images), false)}>
                                    <img src={blockPhotosIcon} />
                                    <span className="tooltip">фотографии</span>
                                  </div>
                                ) : (
                                  <span></span>
                                )}
                                {sortImages(block.plans).length ? (
                                  <div className="block-icon" onClick={this.openModal.bind(this, sortImages(block.plans), false)}>
                                    <img src={blockPlansIcon} />
                                    <span className="tooltip">планировка</span>
                                  </div>
                                ) : (
                                  <span></span>
                                )}
                              </td>
                              <td className="table-area">{block.area}</td>
                              <td className="table-price">{block.price}</td>
                              {this.props.orderType == "rent" &&
                                <td className="table-expense">{block.utilities_costs}</td>
                              }
                              <td className="table-tax">{block.taxes}</td>
                              <td className="table-comment">{block.condition}</td>
                            </tr>)
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div className="additional">
          <div className="additional-top">
            <table>
              <tbody>
                <tr>
                  <td className="additional-information">
                    Дополнительную информацию предлагаем посмотреть на сайте
                  </td>
                  <td>
                    <div className="site"><a href={`http://of.ru/bc/${model.crm_id}`} target="_blank">{/*<span className="bold">of.ru</span>*/}<span className="blue-text">Перейти на сайт{/*/bc/{model.crm_id}*/}</span></a></div>
                  </td>
                  <td className="manager-request">
                    или запросить у Вашего менеджера
                  </td>
                  <td>
                    <div className="manager-id"><div className="id-icon">ID</div><span className="blue-text">{model.id}</span></div>
                  </td>
                  <td className="comment">
                    {model.presentation_comment ? (
                      <span><div className="comment-title light-blue">Комментарий</div><div className="comment-content">{model.presentation_comment}</div></span>
                    ) : (
                      <span></span>
                    )}

                  </td>
                  <td className="download-presentation" onClick={this.downloadPresentation.bind(this)}>
                    <div className="pdf-icon">
                      <img src={pdf} />
                    </div>
                    <div className="pdf-text">
                      <span>Скачать презентацию этого бизнес-центра</span>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="additional-bottom">
            Предложение не является публичной офертой. Вышеуказанные условия могут быть изменены и являются предметом переговоров
          </div>
        </div>
      </div>
    );
  }
}
