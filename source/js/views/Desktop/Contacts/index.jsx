import React, { Component } from 'react';
import Config from 'Config';

import contactsBg from "../../../../assets/img/contacts.png";
import logo from "../../../../assets/img/logo.png"
import bldIcon from "../../../../assets/img/icons/contacts_page/1.png"
import compIcon from "../../../../assets/img/icons/contacts_page/2.png"
import arrowsIcon from "../../../../assets/img/icons/contacts_page/3.png"
import clipboardIcon from "../../../../assets/img/icons/contacts_page/4.png"
import humanIcon from "../../../../assets/img/icons/contacts_page/5.png"

export default class Contacts extends Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(name, index, arrName, event) {
    let broker = this.props.model;
    broker[name] = event.target.value;
    this.props.changedData({broker});
  }

  render() {
    let model = this.props.model;
    let photo = model.photo;

    if (!/http/i.test(photo)) {
      photo = Config.photoAddress + photo;
    }

    return (
      <div className="Сontacts">
        <img className="top-image" src={logo} />
        <img className="bottom-image" src={contactsBg} />
        <div className="contacts-info">
          <div className="contacts-title thin">Контакты</div>
          <div className="contacts-broker">
            <div className="photo-area">
              <img src={photo} />
            </div>
            <div className="text-area">
              <div className="bold"><span>{model.name}</span></div>
              <div className="blue-text thin"><span>{model.post}</span></div>
              <div className="blue-text thin">Центр управления недвижимостью</div>
              <div className="thin"><span>{model.phone}</span></div>
              <div className="thin"><span>{model.email}</span></div>
            </div>
          </div>
          <div className="contacts-title thin">О компании</div>
          <div className="company-info">
            <table className="company-item-bld">
              <tbody>
                <tr>
                  <td className="icon-area">
                    <img src={bldIcon} />
                  </td>
                  <td className="text-area-newline">
                    <span className="bold">10 лет на московском рынке</span> <span className="thin">коммерческой недвижимости</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="company-item-comp">
              <tbody>
                <tr>
                  <td className="icon-area">
                    <img src={compIcon} />
                  </td>
                  <td className="text-area">
                    <span className="thin">Самая</span> <span className="bold">полная и актуальная база</span> <span className="thin">объектов коммерческой недвижимости Москвы</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="company-item-arrows">
              <tbody>
                <tr>
                  <td className="icon-area">
                    <img src={arrowsIcon} />
                  </td>
                  <td className="text-area">
                    <span className="bold">Полный цикл услуг:</span> <span className="thin">подбор объектов недвижимости, отделка, меблировка и переезд</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="company-item-clipboard">
              <tbody>
                <tr>
                  <td className="icon-area">
                    <img src={clipboardIcon} />
                  </td>
                  <td className="text-area">
                    <span className="bold">900 успешных сделок</span> <span className="thin">по аренде и продаже</span>
                  </td>
                </tr>
              </tbody>
            </table>
            <table className="company-item-human">
              <tbody>
                <tr>
                  <td className="icon-area">
                    <img src={humanIcon} />
                  </td>
                  <td className="text-area">
                    <span className="bold">Команда</span> <span className="thin">профессиональных консультантов</span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}
