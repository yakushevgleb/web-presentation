import React, { Component } from 'react';
import Config from 'Config';

import TitlePage from '../TitlePage';
import SuggestedObjects from '../SuggestedObjects';
import OnMap from '../OnMap';
import AboutObject from '../AboutObject';
import Contacts from '../Contacts';
import Footer from '../../../components/Global/Footer';
import Menu from '../../../components/Global/Menu';
import GoogleMaps from '../../../components/Global/GoogleMaps';

let pageCounter = 1;

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      model: null
    }
  }

  componentDidMount() {

    let id = this.props.params["id"]

    fetch(`${Config.address}/api/presentations/data/get_encrypted_landing_data?id=${id}`,
    {
      mode: 'cors'
    })
    .then(res => {
      res.json().then((data)=>{
        console.log(data);
        this.setState({ model: data });
      })
    });

  }

  download(){
    let id = this.props.params["id"]
    window.open(`${Config.address}/api/presentations/data/new_pdf?id=${id}`);
  }

  render() {
    return (
      <div>
      {this.state.model ? (
        <div className='App'>
          <Menu download={this.download.bind(this)}/>
          <div className='A4'><TitlePage model={this.state.model.first_slide} /></div>
          <div className='A4'>
            <SuggestedObjects model={this.state.model.buildings} />
            <Footer pageCounter={pageCounter} broker={this.state.model.broker} />
          </div>
          <div className='A4'>
            <OnMap model={this.state.model.buildings} />
          </div>
          {this.state.model.buildings.map((building, buildingIndex)=>{
            if (buildingIndex < 10) {
              return (<div key={buildingIndex}>
                        <div className='A4'>
                          {building &&
                            <AboutObject buildings={this.state.model.buildings} model={building.building} blocks={building.blocks} orderType={this.state.model.order_type} index={buildingIndex} presId={this.props.params["id"]} />
                          }
                          <Footer pageCounter={pageCounter += 1} broker={this.state.model.broker} />
                        </div>
                      </div>)
            }
          })}
          <div className='A4'><Contacts model={this.state.model.broker} /></div>
        </div>
      ) : (
        <div className='App'>
          <div className="loading">Загрузка...</div>
        </div>
      )}

      </div>
    );
  }
}
