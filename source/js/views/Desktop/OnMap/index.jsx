import React, { Component } from 'react';

import GoogleMaps from "../../../components/Global/GoogleMaps";

import mapTitleIcon from "../../../../assets/img/icons/map_title_icon.png";
import mapIcon from "../../../../assets/img/icons/map_icon.png";

export default class OnMap extends Component {

  goToObj(index) {
    let elem = document.getElementById(`object${index}`);
    elem.scrollIntoView();
  }


  render() {
    let markers = [];
    this.props.model.forEach((bld, index)=>{
      if (index < 10) {
        markers.push({
          position: {
            lat: parseFloat(bld.building.coords.lat),
            lng: parseFloat(bld.building.coords.lon)
          },
          label: {
            text: `${index + 1}`,
            color: "#41abeb",
            fontWeight: "bold",
            bottom: "10px"
          },
          icon: mapIcon
        });
      }
    });

    return (
      <div className="OnMap">
        <div className="map-area">
         <div className="map">
          <GoogleMaps markers={markers} />
         </div>
        </div>
        <div className="text-area">
          <div className="page-title">
             <img src={mapTitleIcon} className="map-title-icon" /><span className="thin">Объекты</span> <span className="bold">на карте</span>
          </div>
          <ul className="points-list">
            {this.props.model.map((bld, index)=>{
              if (index < 10) {
                return (<li key={index} className="point" onClick={this.goToObj.bind(this, index)}><div className="round-marker">{index + 1}</div>{bld.building.name}</li>)
              }
            }
            )}
          </ul>
        </div>
      </div>
    );
  }
}
