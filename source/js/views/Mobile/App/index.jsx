import React, { Component } from 'react';
import Config from 'Config';
import 'whatwg-fetch';

import TitlePage from "../TitlePage"
import SuggestedObjects from "../SuggestedObjects"
import AboutObject from "../AboutObject"
import CompareTable from "../CompareTable"
import Contacts from "../Contacts"
import Header from "../../../components/Global/Header"

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      model: null,
      currentObject: {},
      objectIndex: 0,
      objectOpened: false,
      compareOpened: false,
      suggestedOpened: true,
      scrollToY: 0
    }
    this.openObject = this.openObject.bind(this);
    this.preload = this.preload.bind(this);
  }

  componentWillMount() {

    let id = this.props.params["id"]

    fetch(`${Config.address}/api/presentations/data/get_encrypted_landing_data?id=${id}`,
    {
      mode: 'cors'
    })
    .then(res => {
      res.json().then((data)=>{
        console.log(data);
        this.setState({ model: data });
        data.buildings.forEach(building=>{
          this.preload([building.building.images[0]], 'building');
          building.blocks.forEach(block=>{
            this.preload([block.images[0]], 'block');
            this.preload([block.plans[0]], 'block');
          });
        });
      })
    });

  }

  preload(arr, type) {
    for (let i = 0; i < arr.length; i++) {
        let imageObj = new Image();
        if (type === 'block') {
          imageObj.src = `${Config.address}/api/presentations/data/image_crop?url=${Config.photoAddress + arr[i]}&width=${screen.width}&height=${screen.height/100*90}`;
        } else if (type === 'building') {
          imageObj.src = `${Config.address}/api/presentations/data/image_crop?url=${Config.photoAddress + arr[i]}&width=${screen.width}&height=${250}`;
        } else if (type === 'original') {
          imageObj.src = `${Config.address + arr[i]}`;
        }
    }
  }

  download(){
    let id = this.props.params["id"]
    window.open(`${Config.address}/api/presentations/data/new_pdf?id=${id}`);
  }

  openObject(building, index, pageY) {
    this.setState({
      currentObject: building,
      objectIndex: index,
      objectOpened: true,
      suggestedOpened: false,
      scrollToY: pageY
    });
  }

  openCompare(el) {
    this.setState({
      compareOpened: true,
      suggestedOpened: false,
      scrollToY: el.pageY
    });
  }

  back(){
    this.setState({
      objectOpened: false,
      compareOpened: false,
      suggestedOpened: true
    });
  }

  render() {
    return (
      <div>
      {this.state.model ? (
        <div className='MobileApp'>
          {this.state.suggestedOpened &&
            <TitlePage model={this.state.model.first_slide} />
          }
          {this.state.objectOpened &&
            <div>
              <Header sticky={this.state.objectOpened} phone={this.state.model.broker.phone} />
              <AboutObject model={this.state.currentObject} index={this.state.objectIndex} back={this.back.bind(this)} orderType={this.state.model.order_type} />
            </div>
          }
          {this.state.compareOpened &&
            <div>
              <Header sticky={this.state.compareOpened} phone={this.state.model.broker.phone} />
              <CompareTable model={this.state.model.buildings} back={this.back.bind(this)} />
            </div>
          }
          {this.state.suggestedOpened &&
            <div>
              <Header sticky={!this.state.suggestedOpened} phone={this.state.model.broker.phone} />
              <SuggestedObjects model={this.state.model.buildings} openObject={this.openObject} scrollToY={this.state.scrollToY} />
              <div className="compare-button-background">
                <button className="compare-button thin" onClick={this.openCompare.bind(this)}>Сравнительная таблица</button>
              </div>
            </div>
          }
          <Contacts model={this.state.model.broker} />
        </div>
      ) : (
        <div className='MobileApp'>
          <div className="loading-mobile blue-text">Загрузка...</div>
        </div>
      )}

      </div>
    );
  }
}
