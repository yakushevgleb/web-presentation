import React, { Component } from 'react';
import TextDate from "../../../components/Global/TextDate";

import titleImage from "../../../../assets/img/title_photo.png";
import logo from "../../../../assets/img/logo.png"



let smoothScroll = {
timer: null,

stop: function () {
  clearTimeout(this.timer);
},

scrollTo: function (id, callback) {
  let settings = {
    duration: 700,
    easing: {
      outQuint: function (x, t, b, c, d) {
        return c*((t=t/d-1)*t*t*t*t + 1) + b;
      }
    }
  };
  let percentage;
  let startTime;
  let node = document.getElementById(id);
  let nodeTop = node.offsetTop;
  let nodeHeight = node.offsetHeight;
  let body = document.body;
  let html = document.documentElement;
  let height = Math.max(
    body.scrollHeight,
    body.offsetHeight,
    html.clientHeight,
    html.scrollHeight,
    html.offsetHeight
  );
  let windowHeight = window.innerHeight
  let offset = window.pageYOffset;
  let delta = nodeTop - offset;
  let bottomScrollableY = height - windowHeight;
  let targetY = (bottomScrollableY < delta) ?
    bottomScrollableY - (height - nodeTop - nodeHeight + offset):
    delta;

  startTime = Date.now();
  percentage = 0;

  if (this.timer) {
    clearInterval(this.timer);
  }

  function step () {
    let yScroll;
    let elapsed = Date.now() - startTime;

    if (elapsed > settings.duration) {
      clearTimeout(this.timer);
    }

    percentage = elapsed / settings.duration;

    if (percentage > 1) {
      clearTimeout(this.timer);

      if (callback) {
        callback();
      }
    } else {
      yScroll = settings.easing.outQuint(0, elapsed, offset, targetY, settings.duration);
      window.scrollTo(0, yScroll);
      this.timer = setTimeout(step, 10);
    }
  }

  this.timer = setTimeout(step, 10);
}
};

export default class TitlePage extends Component {

  handleTopClick() {
		smoothScroll.scrollTo('bottom');
	}

	handleBottomClick() {
		smoothScroll.scrollTo('top');
	}

  render() {

    return (
      <div className='MobileTitlePage'>
        <img src={titleImage} className="title-image" />
        <div className="wrapper">
          <div className="grey-border">
          </div>
        </div>
        <div className="title-content">
         <img src={logo} className="logo" />
         <div className="page-title">
          <span className="bold">Офисная</span> <span className="thin">недвижимость</span>
          <div className="small-text thin">
            Презентация для компании "{this.props.model.clientName}"
          </div>
          <span className="date thin"><TextDate /></span>
         </div>
        </div>
        <div className="down-button" onClick={this.handleTopClick.bind(this)}>
          <svg width="100%" height="100%" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
           <g>
            <ellipse stroke="#41abeb" ry="48.5" rx="48.5" id="svg_3" cy="50" cx="50" strokeWidth="1.5" fill="#41abeb"/>
            <path stroke="#ffffff" transform="rotate(180 48.98249816894531,54.11107635498047) " id="svg_10" d="m26.4825,65.56562l22.5,-22.90909l22.5,22.90909l-45,0z" strokeWidth="1.5" fill="#ffffff"/>
           </g>
          </svg>
        </div>
      </div>
    );
  }
}
