import React, { Component } from 'react';
import Config from 'Config';

import Header from "../../../components/Global/Header";
import MobilePhotoViewer from "../../../components/Global/MobilePhotoViewer";
import GoogleMaps from "../../../components/Global/GoogleMaps";

import buildingIcon from "../../../../assets/img/icons/building.png";
import addrIcon from "../../../../assets/img/icons/addr.png";
import metroIcon from "../../../../assets/img/icons/metro.png";
import smallMap from "../../../../assets/img/icons/small-map.png";
import smallMapIcon from "../../../../assets/img/icons/small_map_icon.png";
import plus from "../../../../assets/img/icons/plus.png";
import blockPhotosIcon from "../../../../assets/img/icons/block_photos.png";
import blockPlansIcon from "../../../../assets/img/icons/block_plans.png";
import abc from "../../../../assets/img/icons/abc.png";
import dogovor from "../../../../assets/img/icons/dogovor.png";
import gear from "../../../../assets/img/icons/gear.png";
import goal from "../../../../assets/img/icons/goal.png";
import lift from "../../../../assets/img/icons/lift.png";
import layer from "../../../../assets/img/icons/layer.png";
import photo from "../../../../assets/img/icons/photo.png";
import check from "../../../../assets/img/icons/v.png";
import pen from "../../../../assets/img/icons/pen.png";
import parkingIcon from "../../../../assets/img/icons/parking.png";
import floorsIcon from "../../../../assets/img/icons/bld_icon.png";
import craneIcon from "../../../../assets/img/icons/crane.png";
import rulerIcon from "../../../../assets/img/icons/ruler.png";
import taxIcon from "../../../../assets/img/icons/tax_icon.png";
import general from "../../../../assets/img/icons/general.png";
import comment from "../../../../assets/img/icons/comment.png";

export default class AboutObject extends Component {

  state = {
    dynamicWidth: null,
    dynamicHeight: null,
    mapShowed: false,
    modalOpened: false,
    propsArr: [],
    marker: [{
      position: {
      lat: 55.76,
      lng: 37.64
      },
      icon: smallMapIcon
    }],
    blockImages: []
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    let photoArea = document.querySelector('.photo-area');
    this.setState({
      dynamicWidth: photoArea.clientWidth,
      dynamicHeight: photoArea.clientHeight,
      propsArr: [
        {
          value: this.props.model.building.bc_class,
          icon: abc,
          propName: "Класс"
        },
        {
          value: this.props.model.building.year,
          icon: craneIcon,
          propName: "Год постройки"
        },
        {
          value: this.props.model.building.area,
          icon: goal,
          propName: "Общая площадь"
        },
        {
          value: this.props.model.building.floors,
          icon: floorsIcon,
          propName: "Этажность"
        },
        {
          value: this.props.model.building.lift_company,
          icon: lift,
          propName: this.props.model.building.passenger_elevators
        },
        {
          value: this.props.model.building.land_area,
          icon: rulerIcon,
          fieldName: "land_area",
          propName: "Площадь участка"
        },
        {
          value: this.props.model.building.rent_contract,
          icon: dogovor,
          fieldName: "rent_contract",
          propName: "Договор аренды"
        },
        {
          value: this.props.model.building.taxes_department,
          icon: taxIcon,
          fieldName: "taxes_department",
          propName: "Налоговая"
        }
      ],
      marker: [{
        position: {
        lat: parseFloat(this.props.model.building.coords.lat),
        lng: parseFloat(this.props.model.building.coords.lon)
        },
        icon: smallMapIcon
      }]
    })
  }

  back() {
    this.props.back();
  }

  showMap() {
    this.setState({
      mapShowed: !this.state.mapShowed
    });
  }

  openModal(images){
    let sortedImages = [];
    if(images) {
      images.forEach(el=>{
        if (el.show) {
          sortedImages.push(el.url);
        }
      });
    }
    this.setState({
      modalOpened: !this.state.modalOpened,
      blockImages: sortedImages
    });
  }


  render() {
    let buildings = this.props.model;
    let infrastructures = this.props.model.building.infrastructures.split(',');

    function currencySign(val) {
      switch (val) {
        case 1:
          return (<span>руб.</span>);
        case 2:
          return (<span>&euro;</span>);
        case 3:
          return (<span>$</span>);
        default:
          return (<span>руб.</span>);

      }
    }

    return (
      <div className='MobileAboutObject' id="bottom">
        <div className="back-header">
          <div className="back" onClick={this.back.bind(this)}>
            <svg className="back-button" width="25" height="25" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
              <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
              <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
              <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
            </svg>
            <span className="back-text blue-text">Перейти на главную</span>
          </div>
        </div>
        <div className="about-object">
          <div className="photo-area">
            <MobilePhotoViewer images={this.props.model.building.images} type='building'/>
            <div className="object-class"><span className="class-letter">{this.props.model.building.bc_class}</span></div>
          </div>
          <div className="object-main">
            <table>
              <tbody>
                <tr>
                  <td className="object-name">{this.props.model.building.name}</td>
                  <td className="object-id"><div>№{this.props.index}</div> <div>ID {this.props.model.building.id}</div></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="object-metros">
            {this.props.model.building.stations.map((station, index)=>
              <div className="station" key={index}>
                <div className="metro"><img src={metroIcon} /> {station.name}</div>
                <div className="distance  blue-text">{station.time} мин {station.type}</div>
              </div>
            )}
          </div>
          <div className="object-address">
            <table>
              <tbody>
                <tr>
                  <td className="address">
                    <div><img src={addrIcon} /> {this.props.model.building.address}</div>
                    <div className="show-map">
                      <span onClick={this.showMap.bind(this)}>{this.state.mapShowed ? "Скрыть карту" : "Показать на карте"}</span>
                    </div>
                  </td>
                  <td className="map">
                    <img src={smallMap} onClick={this.showMap.bind(this)} />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="object-map">
            <GoogleMaps markers={this.state.marker} />
          </div>
          <div className="suggested-areas" style={this.state.mapShowed ? {"marginTop": "250px"} : {"marginTop": 0}}>
            <div className="suggested-title">
              <img src={plus} /> <span>Предлагаемые площади</span>
            </div>
            <div className="suggested-floors">
              {this.props.model.blocks.map((block, index)=>
                <table className="suggested-block" key={index}>
                  <tbody>
                    <tr>
                      <td className="decription-column">
                        <div className="bold">{block.floors} этаж</div>
                        <div className="bold">{block.area} м<sup>2</sup></div>
                        <div className="small-text">{block.condition}</div>
                      </td>
                      <td className="photos-column">
                        {block.plans.length ? (
                          <div><img src={blockPlansIcon} onClick={this.openModal.bind(this, block.plans)} /></div>
                        ) : (
                          <div></div>
                        )}
                        {block.images.length ? (
                          <div><img src={blockPhotosIcon} onClick={this.openModal.bind(this, block.images)} /></div>
                        ) : (
                          <div></div>
                        )}
                      </td>
                      <td className="price-column">
                        <div className="small-text bold">{block.price} за м<sup>2</sup> {(this.props.orderType == 'rent') ? "в год" : ""}</div>
                        <div className="small-text">{block.taxes}</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              )}
            </div>
          </div>
          <div className="commercial">
            <div className="commercial-title">
              <img src={pen} /> <span>Коммерческие условия</span>
            </div>
            <div className="commercial-content">
              {this.props.model.building.offer.map((offer, index)=>{
                if(offer.val) {
                  return (
                    <div key={index}><span>{offer.key}</span> <span className="offer-val blue-text">{offer.val}</span></div>
                  )
                }
              }
              )}
              {this.props.model.building.parking_json.length ? (
                <div className="parking">
                  Паркинг
                </div>
              ) : (
                <div></div>
              )}
              {this.props.model.building.parking_json.map((parking, index)=>
                <div key={index}>
                  <img src={parkingIcon} /> <span className="blue-text">{parking.val}</span> <span>{parking.price}</span> <span>{currencySign(parking.cur)}/мес.</span>
                </div>
              )}
            </div>
          </div>
          <div className="general-info">
            <div className="general-title">
              <img src={general} /> <span>Общая информация</span>
            </div>
            <div className="general-content">
              {this.state.propsArr.map((prop, index)=>{
                if(prop.value) {
                  return (<div className="prop" key={index}>
                             <div className="prop-image"><img src={prop.icon} /></div>
                              <div className="prop-name">
                                <div className="blue-text"><span>{prop.value}</span></div>
                                <div className="prop-name-text">{prop.propName}</div>
                              </div>
                            </div>)
                }

              })}
            </div>
          </div>
          <div className="commercial">
            <div className="commercial-title">
              <img src={gear} /> <span>Технические характеристики</span>
            </div>
            <div className="commercial-content">
              {this.props.model.building.features.map((feature, index)=>{
                if(feature.val) {
                  return (
                    <div key={index}><span>{feature.key}</span> <span className="offer-val blue-text">{feature.val}</span></div>
                  )
                }
              }
              )}
            </div>
          </div>
          {infrastructures.length > 1 &&
            <div className="infrastructure">
              <div className="infrastructure-title">
                <img src={check} /> <span>Инфраструктура</span>
              </div>
              <div className="infrastructure-content">
                {infrastructures.map((elem, index)=>
                  <span key={index} className="infrastructure-value blue-text"> {elem} </span>
                )}
              </div>
            </div>
          }
          {this.props.model.building.presentation_comment &&
            <div className="comment">
              <div className="comment-title">
                <img src={comment} /> <span>Комментарий по БЦ</span>
              </div>
              <div className="comment-content">
                <span>{this.props.model.building.presentation_comment}</span>
              </div>
            </div>
          }
          {this.state.modalOpened &&
            <div className="mobile-modal">
              <div className="mobile-modal-background">
              </div>
              <div className="mobile-close-button" onClick={this.openModal.bind(this, null)}>
               <span>+</span>
              </div>
              <div className="mobile-modal-content">
                <MobilePhotoViewer images={this.state.blockImages} type='block' />
              </div>
            </div>
          }
        </div>
      </div>
    );
  }
}
