import React, { Component } from 'react';
import Config from 'Config';

import Header from "../../../components/Global/Header"

import buildingIcon from "../../../../assets/img/icons/building.png";
import addrIcon from "../../../../assets/img/icons/addr.png";
import metroIcon from "../../../../assets/img/icons/metro.png";

export default class SuggestedObjects extends Component {

  constructor(props) {
    super(props);
    this.state = {
      dynamicWidth: null,
      dynamicHeight: null
    };
  }

  componentDidMount() {
    let photoArea = document.querySelector('.photo-area');
    this.setState({
      dynamicWidth: photoArea.clientWidth,
      dynamicHeight: photoArea.clientHeight
    });
    window.scrollTo(0, this.props.scrollToY - 450);
  }

  openObject(building, index, el) {
    this.props.openObject(building, index, el.pageY);
  }


  render() {
    let buildings = this.props.model;

    return (
      <div className='MobileSuggestedObjects' id="bottom">
        <div className="mobile-page-title">
          <img src={buildingIcon} className="bld-icon" /> <span className="thin">Предлагаемые</span> <span className="bold">объекты</span>
        </div>
        {buildings.map((building, index)=>
          <div key={index} className="suggested-object">
            <div className="photo-area">
              <div className="round-number blue-text"><span className="digit">{index + 1}</span></div>
              <div className="photo"><img src={`${Config.address}/api/presentations/data/image_crop?url=${building.building.first_image}&width=${this.state.dynamicWidth + 100}&height=${this.state.dynamicHeight + 100}`} /></div>
            </div>
            <div className="description-area">
              <table className="description">
                <tbody>
                  <tr>
                    <td className="icons-column blue-text">{building.building.bc_class}</td><td>{building.building.name}</td>
                  </tr>
                  <tr>
                    <td className="icons-column"><img src={addrIcon} /></td><td>{building.building.address}</td>
                  </tr>
                  <tr>
                    <td className="icons-column"><img src={metroIcon} /></td><td>{building.building.stations[0].name}</td>
                  </tr>
                </tbody>
              </table>
              <button className="blue-button" onClick={this.openObject.bind(this, building, index + 1)}>Посмотреть офисы</button>
            </div>
          </div>
        )}
      </div>
    );
  }
}
