import React, { Component } from 'react';
import Config from 'Config';

import Header from "../../../components/Global/Header";
import MobilePhotoViewer from "../../../components/Global/MobilePhotoViewer";

import suggestedIcon from "../../../../assets/img/icons/suggested.png";
import blockPhotosIcon from "../../../../assets/img/icons/block_photos.png";
import blockPlansIcon from "../../../../assets/img/icons/block_plans.png";

export default class CompareTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modalOpened: false,
      blockImages: []
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
  }

  back() {
    this.props.back();
  }

  openModal(images){
    let sortedImages = [];
    if(images) {
      images.forEach(el=>{
        if (el.show) {
          sortedImages.push(el.url);
        }
      });
    }
    this.setState({
      modalOpened: !this.state.modalOpened,
      blockImages: sortedImages
    });
  }


  render() {

    return (
      <div className='MobileCompareTable' id="bottom">
        <div className="back-header">
          <div className="back" onClick={this.back.bind(this)}>
            <svg className="back-button" width="25" height="25" viewBox="0 0 193 193" xmlns="http://www.w3.org/2000/svg">
              <ellipse ry="91" rx="91" id="svg_1" cy="96.5" cx="96.5" strokeWidth="8" stroke="#ffffff" fill="#41abeb"/>
              <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_5" y2="99.12634" x2="79.87366" y1="74.5" x1="104.5" strokeWidth="8" fill="none"/>
              <line stroke="#ffffff" strokeLinecap="null" strokeLinejoin="null" id="svg_6" y2="118.00325" x2="104.00325" y1="94.5" x1="80.5" strokeWidth="8" fill="none"/>
            </svg>
            <span className="back-text blue-text">Перейти на главную</span>
          </div>
        </div>

        <div className="compare-title">
          <img src={suggestedIcon} /> <span className="thin">Сравнительная</span> <span className="bold">таблица</span>
        </div>

        <div className="compare-areas">
          {this.props.model.map((building, index)=>
            <div key={index}>
              <div className="compare-building blue-text">
                {building.building.name}
              </div>
                {building.blocks.map((block, index)=>
                  <div key={index} className="compare-floors">
                    <table className="compare-block" key={index}>
                      <tbody>
                        <tr>
                          <td className="decription-column">
                            <div className="bold">{block.floors} этаж</div>
                            <div className="bold">{block.area} м<sup>2</sup></div>
                            <div className="small-text">{block.condition}</div>
                          </td>
                          <td className="photos-column">
                            {block.plans.length ? (
                              <div><img src={blockPlansIcon} onClick={this.openModal.bind(this, block.plans)} /></div>
                            ) : (
                              <div></div>
                            )}
                            {block.images.length ? (
                              <div><img src={blockPhotosIcon} onClick={this.openModal.bind(this, block.images)} /></div>
                            ) : (
                              <div></div>
                            )}
                          </td>
                          <td className="price-column">
                            <div className="small-text bold">{block.price} в месяц</div>
                            <div className="small-text">{block.taxes}</div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                )}
            </div>
          )}
        </div>
        {this.state.modalOpened &&
          <div className="mobile-modal">
            <div className="mobile-modal-background">
            </div>
            <div className="mobile-close-button" onClick={this.openModal.bind(this, null)}>
             <span>+</span>
            </div>
            <div className="mobile-modal-content">
              <MobilePhotoViewer images={this.state.blockImages} type='block'/>
            </div>
          </div>
        }
      </div>

    );
  }
}
