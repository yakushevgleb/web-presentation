import React, { Component } from 'react';
import Config from 'Config';

import logo from "../../../../assets/img/logo.png"
import bldIcon from "../../../../assets/img/icons/contacts_page/1.png"
import compIcon from "../../../../assets/img/icons/contacts_page/2.png"
import arrowsIcon from "../../../../assets/img/icons/contacts_page/3.png"
import clipboardIcon from "../../../../assets/img/icons/contacts_page/4.png"
import humanIcon from "../../../../assets/img/icons/contacts_page/5.png"

export default class Contacts extends Component {

  render() {
    let model = this.props.model;
    let photo = model.photo;

    if (!/http/i.test(photo)) {
      photo = Config.photoAddress + photo;
    }

    return (
      <div className="MobileСontacts">
        <div className="contacts-title thin">Контакты</div>
        <div className="contacts-broker">
          <div className="photo-area">
            <img src={photo} />
          </div>
          <div className="text-area">
            <div className="broker-name bold"><span>{model.name}</span></div>
            <div className="blue-text thin"><span>{model.post}</span></div>
            <div className="blue-text thin">Центр управления недвижимостью</div>
            <div className="thin"><a className="phone-number" href={`tel:${model.phone}`}>{model.phone}</a></div>
            <div className="thin"><a className="phone-number" href="tel:+7 (495) 646-13-46">+7 (495) 646-13-46</a></div>
            <div className="thin"><span>{model.email}</span></div>
          </div>
        </div>
        <div className="contacts-title thin">О компании</div>
        <div className="contacts-broker">
        <div className="text-area">
          <div><img src={bldIcon} /></div>
          <div><span className="bold">10 лет на московском рынке </span><div>коммерческой недвижимости</div></div>
          <div><img src={compIcon} /></div>
          <div>Самая <span className="bold">полная и актуальная база</span> <div>объектов коммерческой недвижимости Москвы</div></div>
          <div><img src={arrowsIcon} /></div>
          <div><span className="bold">Полный цикл услуг:</span> подбор объектов <div>недвижимости, отделка, меблировка и переезд</div></div>
          <div><img src={clipboardIcon} /></div>
          <div><span className="bold">900 успешных сделок</span> по аренде <div>и продаже недвижимости</div></div>
          <div><img src={humanIcon} /></div>
          <div><span className="bold">Команда</span> профессиональных <div>консультантов</div></div>
        </div>
        </div>
        <img className="logo" src={logo} />
        <div className="bottom-triangle"></div>
      </div>
    );
  }
}
